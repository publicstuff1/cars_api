from flask import Flask, jsonify, request, Response
from flask_sqlalchemy import SQLAlchemy

import os

db = SQLAlchemy()

class Car(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(16), nullable=False)
    min_value = db.Column(db.Float, nullable=False)
    entry_date = db.Column(db.String(16), nullable=False)
    description = db.Column(db.String(1024), nullable=False)
    def toDict(self):
        return {
            'id': self.id,
            'category': self.category,
            'min_value': self.min_value,
            'entry_date': self.entry_date,
            'description': self.description
        }
    @staticmethod
    def new(car_dict):
        car = Car(
            category=car_dict['category'], 
            min_value=car_dict['min_value'],
            entry_date=car_dict['entry_date'],
            description=car_dict['description']
        )
        db.session.add(car)
        db.session.commit()
        return car

def create_app():
    app = Flask(__name__)
    with app.app_context():
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///cars.db'
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        if not os.path.exists("cars.db"):
            print("Inicializando o banco!")
            db.create_all()
    return app

app = create_app()

@app.route('/cars', methods=['GET'])
def get_cars():
    cars = Car.query.all()
    ret = []
    for car in cars:
        ret.append(car.toDict())
    return jsonify(ret)

@app.route('/cars/add', methods=['POST'])
def add_car():
    car_data = request.get_json()
    Car.new(car_data)
    return Response(status=201)
