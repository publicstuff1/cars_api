CARS API
========

API http desenvolvida para a prova final de recuperação da disciplina de programação de dispositivos móveis.

COMO EXECUTAR?
==============

1) Baixe o python na página oficinal (python.org) na versão 3. No linux geralmente pode se baixar pelo gerenciador de pacotes padrão ou então simplesmente já vem instalado.
2) Durante a instalação do python na sua máquina certifique-se de ter marcado a opção de instalar para todos os usuários da máquina.
3) Certifique-se de ter instalado o pip na instalação do python.
4) Baixe este repositório.
5) Instale o virtualenv pelo pip usando aplicação terminal (prompt):
	> pip install virtualenv
6) No diretório do seu projeto, execute o comando para criação do ambiente virtual:
	> virtualenv .venv
7) Acesse o ambiente virtual:

	#### No Linux:
	-------------

	Ative o virtualenv do seu projeto
	> source ./.venv/bin/activate

	#### No windows:
	-------------
	Ative o virtualenv do seu projeto
	> .\\.venv\\Scripts\\activate
	
	Caso encontre problemas para executar no linux, tente executar esta linha abaixo antes de ativar o virtualenv:
	> Set-ExecutionPolicy Unrestricted -Scope Process
1) Instale as dependências necessárias para este projeto:
	> pip install -r requirements.txt
9) Execute a aplicação servidora:
	> flask run --port=5000 --host=0.0.0.0

OBSERVAÇÕES
===========

As linhas que seguem serão mostradas após a execução do passo 9. Para parar a execução do servidor pressione (ctrl + c)
```
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on all addresses (0.0.0.0)
   WARNING: This is a development server. Do not use it in a production deployment.
 * Running on http://127.0.0.1:5000
 * Running on http://192.168.100.5:5000 (Press CTRL+C to quit)
```

**Perceba** que o IP que deverá ser utilizado na aplicação móvel localmente é "192.168.100.5" como na última linha do quadro acima.

ROTAS
=====

Neste serviço exitem duas rotas:

1) **get_cars** (/cars): retorna os veículos cadastrados em forma de JSONArray como segue no quadro abaixo:

```
[
	{
		"category": "suv",
		"description": "Toyota Blablabla Ano 2015 - Veículo em bom estado",
		"entry_date": "06/07/2022",
		"id": 1,
		"min_value": 80750.0
	},
	{
		"category": "motocicleta",
		"description": "Honda Blebleble Ano 2010 - Motocicleta em bom estado",
		"entry_date": "06/07/2022",
		"id": 2,
		"min_value": 15000.0
	},
    ...
]
```

1) **add_car** (/cars/add): adiciona veículos na base de dados do servidor. O *body* da requisição http deverá carregar um objeto JSON como segue:

```
{
	"category": "carro", 
	"description": "Fiat Bliblibli Ano 97 - Carro em estado regular",
	"entry_date": "06/07/2022",
	"min_value": 10800.0
}
```